# Programmation concurrente

<div align="center">
<img width="500" height="400" src="Concurrence.jpg">
</div>

## Description du projet

Application console réalisée avec Java SE en LP MIAR à l'IUT de Nantes dans le cadre du module "Programmation Objet" durant l'année 2018-2019 avec un groupe de deux personnes. \
Projet sous forme d'exercices qui mettent en avant la programmation concurrente.
Les exercices sont :
- Arbres binaires de recherche ;
- Les toilettes unisexes ;
- Le dîner des philosophes.

## Acteurs

### Réalisateurs

Ce projet a été réalisé par un groupe de deux étudiants de l'IUT de Nantes :
- Théo MAHAUDA : theo.mahauda@etu.univ-nantes.fr ;
- Mathieu PINEAU : mathieu.pineau@etu.univ-nantes.fr.

### Encadrants

Ce projet fut encadré par un enseignant de l'IUT de Nantes :
- Matthieu PERRIN : matthieu.perrin@univ-nantes.fr.

## Organisation

Ce projet a été agit au sein de l'université de Nantes dans le cadre du module "Programmation Objet" de la LP MIAR

## Date de réalisation

Ce projet a été éxécuté durant l'année 2018 sur la période du module pendant les heures de TP et personnelles à la maison. \
Il a été terminé et rendu le 05/11/2018.

## Technologies, outils et procédés utilisés

Ce projet a été accomplis avec les technologies, outils et procédés suivants :
- Java ;
- Eclipse ;
- Ant ;
- Thread ;
- Deadlock-free ;
- Starvation-free ;
- Protection "Gros grains", "Grains fins".
