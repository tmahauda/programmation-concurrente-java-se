package philosophe;

/**
 * Table où les philosophe pensent et mangent
 * 
 * @author Théo MAHAUDA, Mathieu PINEAU
 * @version 1.0
 */
public class Table implements Runnable
{
	private Philosophe[] philosophes;
	
	/**
	 * Constructeur qui permet d'instancier les philosophes
	 * @param taille le nombre de philosophe autour de la table
	 */
	public Table(int taille)
	{
		this.philosophes = new Philosophe[taille];
		
		for(int i=0; i<taille; i++)
		{
			this.philosophes[i] = new Philosophe("Philosophe " + i);
		}
		
		for(int i=0; i<taille; i++)
		{
			if(i == taille-1)
			{
				this.philosophes[i].setVoisinDroit(this.philosophes[0]);
			}
			else
			{
				this.philosophes[i].setVoisinDroit(this.philosophes[i+1]);
			}
		}
	}

	@Override
	public void run() 
	{
		for(int i=0; i<this.philosophes.length; i++)
		{
			this.philosophes[i].reflechir();
			this.philosophes[i].manger();
			this.philosophes[i].reflechir();
		}
	}
}
