package philosophe;

/**
* Application philosophe
* 
* @author Théo MAHAUDA, Mathieu PINEAU
*/
public class PhilosopheApp 
{
	public static void main(String[] args) 
	{
		Table table = new Table(5);
		Thread[] threads = new Thread[5];
		
		for(int i=0; i<threads.length; i++)
		{
			threads[i] = new Thread(table, "Thread " + i);
			threads[i].start();
		}
	}

}
