package philosophe;

/**
 * Chaque philosophe possède une baguette. Il occupe la majorité de son temps a réfléchir. 
 * Mais entre deux périodes d’intense réflexion, un philosophe a besoin de se nourrir. 
 * Pour manger, il doit commencer par prendre deux baguettes : celui qu'il possède et 
 * celui de son voisin de droite. Il peut alors manger un peu de riz. Une fois rassasié, 
 * il repose les deux baguettes.
 * 
 * @author Théo MAHAUDA, Mathieu PINEAU
 * @version 1.0
 */
public class Philosophe 
{
	/**
	 * Le nom du philosophe
	 */
	private String nom;
	
	/**
	 * Le voisin de droit du philosophe courant
	 */
	private Philosophe voisinDroit;
	
	/**
	 * La baguette permettant au philosophe de manger
	 */
	private Baguette baguette;
	
	/**
	 * Constructeur qui permet d'instancier un philosophe
	 * @param nom le nom du philosophe
	 */
	public Philosophe(String nom) 
	{
		this.voisinDroit = null;
		this.nom = nom;
		this.baguette = new Baguette();
	}

	/**
	 * Méthode qui permet à un philosophe de réfléchir pendant 0,5s
	 */
	public synchronized void reflechir()
	{
		System.out.println(this.nom + " est en train de réfléchir");
		
		try 
		{
			Thread.sleep(500);
		} catch (InterruptedException e) {}
		
		System.out.println(this.nom + " a finis de réfléchir");
	}
	
	/**
	 * Méthode qui permet à un philosophe de manger pendant 0,5s
	 */
	public synchronized void manger()
	{
		
		if(this.baguette.getNumero() < this.voisinDroit.baguette.getNumero())
		{
			System.out.println(this.nom + " tente de prendre sa baguette");
			
			//Il pose son verrou sur sa baguette
			synchronized(this.baguette)
			{
				System.out.println(this.nom + " est en train de prendre sa baguette");
				System.out.println(this.nom + " tente de prendre la baguette de son voisin " + this.voisinDroit.nom);
				
				//Il pose son verrou sur la baguette de son voisin
				synchronized(this.voisinDroit.baguette)
				{
					System.out.println(this.nom + " est en train de prendre la baguette de son voisin " + this.voisinDroit.nom);
					System.out.println(this.nom + " est en train de manger");
					
					try 
					{
						Thread.sleep(500);
					} catch (InterruptedException e) {}
					
					System.out.println(this.nom + " a fini de manger. Ils déposent les baguettes");
				}
			}
		}
		else
		{
			System.out.println(this.nom + " tente de prendre la baguette de son voisin " + this.voisinDroit.nom);
			
			//Il pose son verrou sur la baguette de son voisin
			synchronized(this.voisinDroit.baguette)
			{
				System.out.println(this.nom + " est en train de prendre la baguette de son voisin " + this.voisinDroit.nom);
				System.out.println(this.nom + " tente de prendre sa baguette");
				
				//Il pose son verrou sur sa baguette
				synchronized(this.baguette)
				{
					System.out.println(this.nom + " est en train de prendre sa baguette");
					System.out.println(this.nom + " est en train de manger");
					
					try 
					{
						Thread.sleep(500);
					} catch (InterruptedException e) {}
					
					System.out.println(this.nom + " a fini de manger. Ils déposent les baguettes");
				}
			}
		}
	}



	/**
	 * @return the voisinDroit
	 */
	public Philosophe getVoisinDroit() {
		return voisinDroit;
	}

	/**
	 * @param voisinDroit the voisinDroit to set
	 */
	public void setVoisinDroit(Philosophe voisinDroit) {
		this.voisinDroit = voisinDroit;
	}

	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * @return the baguette
	 */
	public Baguette getBaguette() {
		return baguette;
	}

	/**
	 * @param baguette the baguette to set
	 */
	public void setBaguette(Baguette baguette) {
		this.baguette = baguette;
	}

	@Override
	public String toString() {
		return "Philosophe [nom=" + nom + ", baguette=" + baguette + ", voisinDroit=" + voisinDroit + "]";
	}
		
}
