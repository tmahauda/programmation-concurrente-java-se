package philosophe;

/**
 * Permettre à un philosophe de manger avec la baguette
 * 
 * @author Théo MAHAUDA, Mathieu PINEAU
 * @version 1.0
 */
public class Baguette 
{
	private int numero;
	private static int num = 0;

	/**
	 * Constructeur qui permet d'instancier une baguette
	 */
	public Baguette() {
		num++;
		this.numero = num;
	}

	/**
	 * @return the numero
	 */
	public int getNumero() {
		return numero;
	}

	/**
	 * @param numero the numero to set
	 */
	public void setNumero(int numero) {
		this.numero = numero;
	}

}
