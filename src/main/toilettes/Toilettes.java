package toilettes;

import java.util.LinkedList;

/**
 * 
 * Les toilettes unisexes peuvent être utilisées par les hommes et par les femmes, sous les contraintes suivantes :
 * 1. il ne doit jamais y avoir en même temps des hommes et des femmes aux toilettes ;
 * 2. il ne doit jamais y avoir plus de trois personnes simultanément aux toilettes.
 * 
 * @author Théo MAHAUDA, Mathieu PINEAU
 * @version 1.0
 */
public class Toilettes 
{
	/**
	 * Compteur qui permet de verifier combien il y a d'hommes dans les toilettes
	 */
	private int cptHommes;
	
	/**
	 * Compteur qui permet de verifier combien il y a de femmes dans les toilettes
	 */
	private int cptFemmes;
	
	/**
	 * Nombre de personne que les toilettes peuvent acceuillir
	 */
	private int capacite;
	
	/**
	 * File des toilettes
	 */
	private LinkedList<Personne> personnesEntrer;
	
	/**
	 * File d'attente
	 */
	private LinkedList<Personne> personnesAttendre;
	
	/**
	 * Constructeur qui permet d'instancier les toilettes selon une capacité qu'elles peuvent accueillir
	 * @param capacite le nombre de personne qui peut y entrer
	 */
	public Toilettes(int capacite)
	{
		this.cptHommes = 0;
		this.cptFemmes = 0;
		this.capacite = capacite;
		this.personnesEntrer = new LinkedList<Personne>();
		this.personnesAttendre = new LinkedList<Personne>();
	}
	
	/**
	 * Méthode qui permet d'afficher l'état des attributs lorsque
	 * - une personne entre des toilettes
	 * - une personne sort des toilettes
	 */
	public void afficher()
	{
		System.out.println("Toilette : " + this.personnesEntrer);
		System.out.println("File d'attente : " + this.personnesAttendre);
		System.out.println("Compteur hommes : " + this.cptHommes);
		System.out.println("Compteur femmes : " + this.cptFemmes);
	}
	
	/**
	 * Methode qui permet de faire rentrer une personne en tete de file d'attente
	 * dans la file des toilettes
	 * @return vrai ou faux
	 */
	private boolean entrer()
	{
		//Si il y a une file d'attente
		if(!this.personnesAttendre.isEmpty())
		{
			//On prend la personne en tete de file
			Personne p = this.personnesAttendre.peek();
			this.supprimer(p, this.personnesAttendre, "file d'attente");
			
			//On essaye de faire entrer la personne dans les toilettes
			if(!this.entrer(p))
			{
				this.personnesAttendre.remove(p);
				
				//On replace la personne en tete de file
				this.personnesAttendre.addFirst(p);
				
				return false;
			}
			else
			{
				return true;
			}
		}
		else
		{
			System.out.println("Aucune personne dans file d'attente");
			return false;
		}
	}
	
	/**
	 * Méthode qui permet de faire entrer une personne extérieure
	 * dans la file des toilettes
	 * @param p la personne à faire rentrer dans les toilettes
	 * @return vrai ou faux
	 */
	public synchronized boolean entrer(Personne p)
	{
		boolean entrer = false;
		
		if(p.getSexe() == 'M')
		{
			entrer = this.entrer(p, this.cptHommes < this.capacite, this.cptFemmes == 0);
		}
		else
		{
			entrer = this.entrer(p, this.cptFemmes < this.capacite, this.cptHommes == 0);
		}
		
		this.afficher();
		
		return entrer;
	}
	
	/**
	 * Méthode qui permet de faire entrer une personne dans les toilettes si :
	 * - il n'y a pas de queue
	 * - il reste de la place dans les toilettes
	 * - les toilettes sont unisexes
	 * @param p la personne à faire entrer dans les toilettes
	 * @param place pour vérifier si il reste de la place dans les toilettes
	 * @param unisexe pour vérifier si la personne à le même sexe que les personnes qui sont dans les toilettes
	 * @return vrai ou faux
	 */
	private boolean entrer(Personne p, boolean place, boolean unisexe)
	{
		if(this.personnesAttendre.isEmpty() || p.isAttendre())
		{
			if(place)
			{
				if(unisexe)
				{
					if(this.ajouter(p, this.personnesEntrer, "toilette"))
					{
						p.setAttendre(false);
						
						if(p.getSexe() == 'M')
						{
							this.cptHommes++;
						}
						else
						{
							this.cptFemmes++;
						}
						
						notifyAll();
						return true;
					}
					else
					{
						return false;
					}
				}
				else
				{
					System.out.println(p + " ne peut pas entrer car non unisexe");
					this.ajouter(p, this.personnesAttendre, "file d'attente");
					p.setAttendre(true);
					return false;
				}
			}
			else
			{
				System.out.println(p + " ne peut pas entrer car plus de place");
				this.ajouter(p, this.personnesAttendre, "file d'attente");
				p.setAttendre(true);
				return false;
			}
		}
		else
		{
			System.out.println(p + " ne peut pas entrer car il y a une queue");
			this.ajouter(p, this.personnesAttendre, "file d'attente");
			p.setAttendre(true);
			return false;
		}
	}
	
	/**
	 * Méthode qui permet d'ajouter une personne en queue d'une file de type :
	 * - file des toilettes
	 * - file d'attente
	 * @param p la personne à ajouter en queue de la file
	 * @param file pour ajouter la personne
	 * @param type le type de file
	 * @return vrai ou faux
	 */
	private boolean ajouter(Personne p, LinkedList<Personne> file, String type)
	{
		System.out.println(p + " s'apprete à entrer dans " + type);
		
		if(!file.contains(p))
		{
			System.out.println(p + " entre dans " + type);
			file.add(p);
			return true;
		}
		else
		{
			System.out.println(p + " est déjà présent dans " + type);
			return false;
		}
	}
	
	/**
	 * Méthode qui permet de supprimer une personne en tete d'une file de type :
	 * - file des toilettes
	 * - file d'attente
	 * @param p la personne à supprimer en tete de la file
	 * @param file pour supprimer la personne
	 * @param type le type de file
	 * @return vrai ou faux
	 */
	private boolean supprimer(Personne p, LinkedList<Personne> file, String type)
	{
		System.out.println(p + " s'apprete à sortir " + type);
		
		if(file.contains(p))
		{
			System.out.println(p + " sort " + type);
			file.remove(p);
			return true;
		}
		else
		{
			System.out.println(p + " n'est pas présent dans " + type);
			return false;
		}
	}
	
	/**
	 * Méthode qui permet de faire sortir une personne des toilettes
	 * @param p la personne à faire sortir
	 * @return vrai ou faux
	 */
	public synchronized boolean sortir(Personne p)
	{
		//Tant que la personne n'est pas encore entrer dans 
		//les toilettes, on patiente
		while(!this.supprimer(p, this.personnesEntrer, "toilette"))
		{
			try {
				wait();
			} catch (InterruptedException e) {
				return false;
			}
		}
		
		if(p.getSexe() == 'M')
		{
			this.cptHommes--;
		}
		else
		{
			this.cptFemmes--;
		}
		
		//On regarde dans la file d'attente
		while(this.entrer());
		
		this.afficher();
		
		return true;

	}

	/**
	 * @return the cptHommes
	 */
	public int getCptHommes() {
		return cptHommes;
	}

	/**
	 * @param cptHommes the cptHommes to set
	 */
	public void setCptHommes(int cptHommes) {
		this.cptHommes = cptHommes;
	}

	/**
	 * @return the cptFemmes
	 */
	public int getCptFemmes() {
		return cptFemmes;
	}

	/**
	 * @param cptFemmes the cptFemmes to set
	 */
	public void setCptFemmes(int cptFemmes) {
		this.cptFemmes = cptFemmes;
	}

	/**
	 * @return the capacite
	 */
	public int getCapacite() {
		return capacite;
	}

	/**
	 * @param capacite the capacite to set
	 */
	public void setCapacite(int capacite) {
		this.capacite = capacite;
	}

	/**
	 * @return the personnesEntrer
	 */
	public LinkedList<Personne> getPersonnesEntrer() {
		return personnesEntrer;
	}

	/**
	 * @param personnesEntrer the personnesEntrer to set
	 */
	public void setPersonnesEntrer(LinkedList<Personne> personnesEntrer) {
		this.personnesEntrer = personnesEntrer;
	}

	/**
	 * @return the personnesAttendre
	 */
	public LinkedList<Personne> getPersonnesAttendre() {
		return personnesAttendre;
	}

	/**
	 * @param personnesAttendre the personnesAttendre to set
	 */
	public void setPersonnesAttendre(LinkedList<Personne> personnesAttendre) {
		this.personnesAttendre = personnesAttendre;
	}

	@Override
	public String toString() 
	{
		return "Toilette [cptHommes=" + cptHommes + ", cptFemmes=" + cptFemmes + ", capacite=" + capacite
				+ ", personnesEntrer=" + personnesEntrer + ", personnesAttendre=" + personnesAttendre + "]";
	}

}
