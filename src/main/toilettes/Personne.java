package toilettes;

/**
 * Personne
 * 
 * @author Théo MAHAUDA, Mathieu PINEAU
 * @version 1.0
 */
public class Personne 
{
	/**
	 * Masculin ou féminin
	 */
	private char sexe;
	
	/**
	 * Le nom de la personne
	 */
	private String nom;
	
	/**
	 * La personne a-t-elle attendu dans la file d'attente ?
	 */
	private boolean attendre;
	
	/**
	 * Constructeur qui permet d'instancier une personne par son nom et son sexe
	 * @param sexe le sexe masculin 'M' ou féminin 'F'
	 * @param nom le nom de la personne
	 */
	public Personne(char sexe, String nom) 
	{
		this.sexe = sexe;
		this.nom = nom;
		this.attendre = false;
	}
	
	/**
	 * @return the sexe
	 */
	public char getSexe() {
		return sexe;
	}
	/**
	 * @param sexe the sexe to set
	 */
	public void setSexe(char sexe) {
		this.sexe = sexe;
	}
	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}
	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	/**
	 * @return the attendre
	 */
	public boolean isAttendre() {
		return attendre;
	}
	/**
	 * @param attendre the attendre to set
	 */
	public void setAttendre(boolean attendre) {
		this.attendre = attendre;
	}
	
	@Override
	public String toString() {
		return "Personne [nom=" + nom + ", sexe=" + sexe + ", attendre=" + attendre + "]";
	}
	
}
