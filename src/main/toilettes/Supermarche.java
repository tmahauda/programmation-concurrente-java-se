package toilettes;

/**
 * Supermarché dans lequel il y a des personnes qui veulent aller aux toilettes
 * 
 * @author Théo MAHAUDA, Mathieu PINEAU
 * @version 1.0
 */
public class Supermarche implements Runnable
{
	/**
	 * Les toilettes du supermarché
	 */
	private Toilettes toilette;
	
	/**
	 * Les personnes s'apprétant à entrer et à sortir des toilettes
	 */
	private Personne[] personnes;
	
	/**
	 * Constructeur du supermarché
	 * @param toilette les toilettes du supermarché
	 * @param nombre le nombre de personne présente dans le supermarché
	 */
	public Supermarche(Toilettes toilette, int nombre)
	{
		this.toilette = toilette;
		this.personnes = new Personne[nombre];
		
		for(int i=0; i<nombre; i++)
		{
			if(i%2 == 0)
			{
				this.personnes[i] = new Personne('M', "Personne " + i);
			}
			else
			{
				this.personnes[i] = new Personne('F', "Personne " + i);
			}
		}
	}

	@Override
	public void run() 
	{
		for(int i=0; i<this.personnes.length; i++)
		{
			this.personnes[i].setNom(Thread.currentThread().getName() + " " + this.personnes[i].getNom());
			this.toilette.entrer(this.personnes[i]);
			try 
			{
				Thread.sleep(500);
			} catch (InterruptedException e) {}
			this.toilette.sortir(this.personnes[i]);
		}
	}
}
