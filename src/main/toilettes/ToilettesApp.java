package toilettes;

/**
* Application toilettes
* 
* @author Théo MAHAUDA, Mathieu PINEAU
*/
public class ToilettesApp 
{

	public static void main(String[] args) 
	{
		Toilettes toilette = new Toilettes(3);
		Thread[] threads = new Thread[5];
		
		for(int i=0; i<threads.length; i++)
		{
			threads[i] = new Thread(new Supermarche(toilette, 10), "Thread " + i);
			threads[i].start();
		}
	}

}
