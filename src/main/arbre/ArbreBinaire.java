package arbre;

/**
* Arbre binaire
* 
* @author Théo MAHAUDA, Mathieu PINEAU
*/
public class ArbreBinaire implements Runnable
{
	/**
	 * Noeud qui permet de représenter l'arbre : la racine
	 */
	private NoeudBinaire racine;
	
	/**
	 * Le nom du thread qui manipule la racine
	 */
	private String nom;
	
	
	/**
	 * Constructeur qui permet d'instancier l'arbre
	 * @param racine la racine qui représente l'arbre
	 * @param nom le nom du thread
	 */
	public ArbreBinaire(NoeudBinaire racine, String nom)
	{
		this.racine = racine;
		this.nom = nom;
	}
	
	/**
	 * Méthode qui permet de vérifier la présence de la valeur x dans l'arbre
	 * @param x la valeur à verifier
	 */
	public void contains(int x)
	{
		System.out.println(this.nom + " => contains(" + x + ") : " + this.racine.contains(x));
	}
	
	/**
	 * Méthode qui permet d'insérer la valeur x dans l'arbre
	 * @param x la valeur à insérer
	 */
	public void insert(int x)
	{
		System.out.println(this.nom + " => insert(" + x + ") : " + this.racine.insert(x)); 
	}
	
	/**
	 * Méthode qui permet de supprimer la valeur x dans l'arbre
	 * @param x la valeur à supprimer
	 */
	public void delete(int x)
	{
		System.out.println(this.nom + " => delete(" + x + ") : " + this.racine.delete(x)); 
	}
	
	/**
	 * Méthode qui permet d'afficher l'arbre
	 */
	public void display()
	{
		System.out.println(this.nom + " => display() : " + this.racine.display() + "\n" + this.racine.toString());  
	}

	@Override
	public void run() 
	{
		//				
		//				 	  10
		//				/		   	 \
		//			  5			   	 15
		//			/	\		   /	\
		//		  4		 6	      14	16
		//		/		  \		  /		 \
		//	  2		  	  8		 12		 18
		//	/	\		/	\  /	\	/	\
		//	1	3		7	9 11	13 17	20
		
				this.display();
				
				//INSERT
				this.insert(5);
				this.insert(6);
				this.insert(8);
				this.insert(7);
				this.insert(9);
				this.insert(4);
				this.insert(2);
				this.insert(1);
				this.insert(3);
				this.insert(15);
				this.insert(16);
				this.insert(18);
				this.insert(17);
				this.insert(20);
				this.insert(14);
				this.insert(12);
				this.insert(11);
				this.insert(13);
				
				this.display();
				
				//CONTAINS
				this.contains(5);
				this.contains(6);
				this.contains(8);
				this.contains(7);
				this.contains(9);
				this.contains(4);
				this.contains(2);
				this.contains(1);
				this.contains(3);
				this.contains(15);
				this.contains(16);
				this.contains(18);
				this.contains(17);
				this.contains(20);
				this.contains(14);
				this.contains(12);
				this.contains(11);
				this.contains(13);
				
				this.display();
				
				//DELETE
				this.delete(5);
				this.delete(6);
				this.delete(8);
				this.delete(7);
				this.delete(9);
				this.delete(4);
				this.delete(2);
				this.delete(1);
				this.delete(3);
				this.delete(15);
				this.delete(16);
				this.delete(18);
				this.delete(17);
				this.delete(20);
				this.delete(14);
				this.delete(12);
				this.delete(11);
				this.delete(13);
				
				this.display();

	}
}