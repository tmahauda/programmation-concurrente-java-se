package arbre;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.TreeSet;

/**
* Structure de données permettant de stocker des valeurs sous forme 
* d’arbres. Chaque noeud possède une valeur, et deux fils qui sont 
* eux-mêmes des noeuds binaires de recherche, tel que le fils gauche 
* contient les valeurs plus petites que valeur et le fils droit contient 
* les valeurs plus grandes que valeur
* 
* @author Théo MAHAUDA, Mathieu PINEAU
* @version 1.0
*/
public class NoeudBinaire
{
	/**
	 * La valeur du noeud courant
	 */
	private int valeur;
	
	/**
	 * Le noeud pere du noeud courant
	 */
	private NoeudBinaire pere;
	
	/**
	 * Fils gauche contenant les valeurs plus petites que valeur
	 */
	private NoeudBinaire filsGauche;
	
	/**
	 * Fils droit contenant les valeurs plus grandes que valeur
	 */
	private NoeudBinaire filsDroit;
	
	/**
	 * Le numéro du noeud courant pour éviter les deadlocks selon le théorème
	 */
	private int numero;
	
	/**
	 * Le nombre de noeud instancié pour incrémenter le numéro
	 */
	private static int nombreNoeud = 0;
	
	/**
	 * Constructeur qui permet d'instancier un noeud
	 * @param valeur la valeur du noeud
	 * @param pere le noeud pere du noeud
	 * @param filsGauche le noeud fils gauche
	 * @param filsDroit le noeud fils droit
	 */
	public NoeudBinaire(int valeur, NoeudBinaire pere, NoeudBinaire filsGauche, NoeudBinaire filsDroit)
	{
		this.valeur = valeur;
		this.pere = pere;
		this.filsGauche = filsGauche;
		this.filsDroit = filsDroit;
		
		nombreNoeud++;
		this.numero = nombreNoeud;
	}
	
	/**
	 * Constructeur qui permet d'instancier une feuille
	 * @param valeur la valeur de la feuille
	 * @param pere le noeud pere de la feuille
	 */
	public NoeudBinaire(int valeur, NoeudBinaire pere)
	{
		this(valeur, pere, null, null);
	}
	
	/**
	 * Constructeur qui permet d'instancier une racine
	 * @param valeur de la racine
	 */
	public NoeudBinaire(int valeur)
	{
		this(valeur, null, null, null);
	}

	private NoeudBinaire getFils(String fils)
	{
		Class<?> clazz = this.getClass();
		try 
		{
			Method method = clazz.getMethod("get"+fils, (Class<?>[]) null);
			return (NoeudBinaire)method.invoke(this, (Object[]) null);
		}
		//catch getMethod
		catch (NoSuchMethodException | SecurityException e) 
		{
			return null;
		}
		//catch invoke
		catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) 
		{
			return null;
		}
	}
	
	private void setFils(String fils, NoeudBinaire noeudFils)
	{
		Class<?> clazz = this.getClass();
		try 
		{
			Method method = clazz.getMethod("set"+fils, new Class<?>[] {NoeudBinaire.class});
			method.invoke(this, new Object[] {noeudFils});
		}
		//catch getMethod
		catch (NoSuchMethodException | SecurityException e) 
		{

		}
		//catch invoke
		catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) 
		{

		}
	}
	
	/**
	 * Methode qui renvoit le noeud contenant
	 * la valeur passée en paramètre en cherchant dans l'arbre
	 * @param x la valeur à rechercher
	 * @return noeud qui contient la valeur x ou null
	 */
	public NoeudBinaire search(int x)
	{	
		if(this.getValeur() == x)
		{
			return this;
		}
		else
		{	
			if(x > this.getValeur())
			{
				return this.search(x, "FilsDroit");
			}
			else
			{
				return this.search(x, "FilsGauche");
			}
		}
	}
	
	/**
	 * Methode qui renvoit le noeud contenant
	 * la valeur passée en paramètre en cherchant dans
	 * un noeud fils
	 * @param x la valeur à rechercher
	 * @param fils le noeud fils à explorer
	 * @return noeud qui contient la valeur x ou null
	 */
	protected NoeudBinaire search(int x, String fils)
	{
		NoeudBinaire noeudFils = this.getFils(fils);
		
		if(noeudFils != null) 
		{
			return noeudFils.search(x);
		}
		else 
		{
			return null;
		}
	}
	
	/**
	 * Methode qui permet de savoir si une valeur existe dans l'arbre
	 * @see NoeudBinaire#search(int)
	 * @param x la valeur à vérifier
	 * @return vrai ou faux
	 */
	public boolean contains(int x)
	{
		return this.search(x) != null;
	}
	
	/**
	 * Methode qui permet d'insérer une valeur dans l'arbre
	 * @param x la valeur à ajouter
	 * @return vrai ou faux
	 */
	public boolean insert(int x)
	{
		if(x > this.getValeur())
		{
			return this.insert(x, "FilsDroit");
		}
		else if(x < this.getValeur())
		{
			return this.insert(x, "FilsGauche");
		}
		else
		{
			return false;
		}
	}

	/**
	 * Methode qui permet d'insérer une valeur dans un noeud fils
	 * @param x la valeur à ajouter
	 * @param fils à explorer pour insérer la valeur
	 * @return vrai ou faux
	 */
	protected boolean insert(int x, String fils)
	{	
		NoeudBinaire noeudFils = this.getFils(fils);
		
		if(noeudFils != null)
		{
			return noeudFils.insert(x);
		}
		else
		{
			this.setFils(fils, new NoeudBinaire(x, this));
			return true;
		}
	}
	
	/**
	 * Methode qui permet de supprimer une valeur dans l'arbre
	 * @param x la valeur à supprimer
	 * @return vrai ou faux
	 */
	public boolean delete(int x)
	{			
			//On récupère le noeud à supprimer
			NoeudBinaire noeudASupprimer = this.search(x);
			
			//Si le noeud à supprimer n'existe pas et que ce n'est pas la racine
			if(noeudASupprimer != null && noeudASupprimer.getPere() != null)
			{
				//On récupère toutes les valeurs des fils droit et gauche
				//du noeud à supprimer dans une liste ordonnée sans doublon
				TreeSet<Integer> list = noeudASupprimer.display();
				list.remove(noeudASupprimer.getValeur());

				//On ajoute les valeurs par ordre croissant
				if(this.delete(x, noeudASupprimer)) 
				{
					for(int valeur : list)
					{
						this.insert(valeur);
					}
					
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
	}

	/**
	 * Methode qui permet de supprimer un noeud de l'arbre
	 * @param x la valeur à supprimer
	 * @param noeudASupprimer le noeud à supprimer de l'arbre
	 * @return vrai ou faux
	 */
	protected boolean delete(int x, NoeudBinaire noeudASupprimer)
	{
		//On supprimer le noeud à supprimer depuis le noeud pere
		//Soit la valeur trouvée est dans la branche droite du noeud pere
		//Soit la valeur trouvée est dans la branche gauche du noeud pere
		if(noeudASupprimer.getPere().getFilsDroit() != null && 
		   noeudASupprimer.getPere().getFilsDroit().getValeur() == x)
		{
			noeudASupprimer.getPere().setFilsDroit(null);
			return true;
		}
		else if(noeudASupprimer.getPere().getFilsGauche() != null && 
				noeudASupprimer.getPere().getFilsGauche().getValeur() == x)
		{
			noeudASupprimer.getPere().setFilsGauche(null);
			return true;
		}
		else
		{
			return false;
		}
	}
	
	/**
	 * Methode qui permet de récupérer les valeurs par ordre croissant
	 * sans doublon de l'arbre
	 * @return les valeurs ordonnées par ordre croissant sans doublon
	 */
	public TreeSet<Integer> display()
	{
		TreeSet<Integer> list = new TreeSet<Integer>();
		
		list.add(this.getValeur());
		list.addAll(this.display("FilsDroit"));
		list.addAll(this.display("FilsGauche"));
		
		return list;
	}

	/**
	 * Methode qui permet de récupérer les valeurs par ordre croissant
	 * sans doublon d'un noeud fils
	 * @param fils le noeud fils à explorer
	 * @return les valeurs ordonnées par ordre croissant sans doublon
	 */
	protected TreeSet<Integer> display(String fils)
	{
		NoeudBinaire noeudFils = this.getFils(fils);
		
		TreeSet<Integer> list = new TreeSet<>();
		
		if(noeudFils != null)
		{
			list.addAll(noeudFils.display());
		}
		
		return list;
	}
	
	/**
	 * @return the valeur
	 */
	public int getValeur() {
		return valeur;
	}

	/**
	 * @param valeur the valeur to set
	 */
	public void setValeur(int valeur) {
		this.valeur = valeur;
	}

	/**
	 * @return the pere
	 */
	public NoeudBinaire getPere() {
		return pere;
	}

	/**
	 * @param pere the pere to set
	 */
	public void setPere(NoeudBinaire pere) {
		this.pere = pere;
	}

	/**
	 * @return the filsGauche
	 */
	public NoeudBinaire getFilsGauche() {
		return filsGauche;
	}

	/**
	 * @param filsGauche the filsGauche to set
	 */
	public void setFilsGauche(NoeudBinaire filsGauche) {
		this.filsGauche = filsGauche;
	}

	/**
	 * @return the filsDroit
	 */
	public NoeudBinaire getFilsDroit() {
		return filsDroit;
	}

	/**
	 * @param filsDroit the filsDroit to set
	 */
	public void setFilsDroit(NoeudBinaire filsDroit) {
		this.filsDroit = filsDroit;
	}

	/**
	 * @return the numero
	 */
	public int getNumero() {
		return numero;
	}

	/**
	 * @param numero the numero to set
	 */
	public void setNumero(int numero) {
		this.numero = numero;
	}

	@Override
	public int hashCode() 
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + valeur;
		return result;
	}

	@Override
	public boolean equals(Object obj) 
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NoeudBinaire other = (NoeudBinaire) obj;
		if (valeur != other.valeur)
			return false;
		return true;
	}
	
	@Override
	public String toString()
	{
		String valGauche = "NULL";
		String valDroit = "NULL";
		String resultat = "";
		
		if(this.getFilsGauche() != null)
		{
			valGauche = this.getFilsGauche().toString();
		}
		
		if(this.getFilsDroit() != null)
		{
			valDroit = this.getFilsDroit().toString();
		}
		
		resultat += "N = " + this.getValeur();
		resultat += " [FG : " + valGauche;
		resultat += "; FD : " + valDroit + "]";
		
		return resultat;
	}
}
