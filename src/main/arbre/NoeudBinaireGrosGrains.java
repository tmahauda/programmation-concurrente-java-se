package arbre;

import java.util.TreeSet;

/**
 * Structure de données protéger par un verrou : l'arbre
􏰀 * Chaque méthode est entièrement protégée par le verrou
 *
 * @author Théo MAHAUDA, Mathieu PINEAU
 * @version 1.0
 */
public class NoeudBinaireGrosGrains extends NoeudBinaire
{
	
	public NoeudBinaireGrosGrains(int valeur)
	{
		super(valeur);
	}
	
	public NoeudBinaireGrosGrains(int valeur, NoeudBinaire pere)
	{
		super(valeur, pere);
	}
	
	public NoeudBinaireGrosGrains(int valeur, NoeudBinaire pere, NoeudBinaire filsGauche, NoeudBinaire filsDroit)
	{
		super(valeur, pere, filsGauche, filsDroit);
	}
	
	@Override
	public synchronized NoeudBinaire search(int x)
	{	
		return super.search(x);
	}
	
	@Override
	public synchronized boolean contains(int x)
	{
		return super.contains(x);
	}
	
	@Override
	public synchronized boolean insert(int x)
	{
		return super.insert(x);
	}
	
	@Override
	public synchronized boolean delete(int x)
	{			
		return super.delete(x);
	}

	@Override
	public synchronized TreeSet<Integer> display()
	{
		return super.display();
	}

}
