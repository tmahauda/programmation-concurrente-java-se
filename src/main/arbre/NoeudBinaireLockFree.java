package arbre;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Structure de données protéger par aucun verrou : lock free
􏰀 * Utilise le compareAndSet()
 *
 * @author Théo MAHAUDA, Mathieu PINEAU
 * @version 1.0
 */
public class NoeudBinaireLockFree extends NoeudBinaire
{
	private AtomicReference<NoeudBinaire> pere;
	private AtomicReference<NoeudBinaire> filsGauche;
	private AtomicReference<NoeudBinaire> filsDroit;
	
	public NoeudBinaireLockFree(int valeur, NoeudBinaire pere, NoeudBinaire filsGauche, NoeudBinaire filsDroit)
	{
		super(valeur, pere, filsGauche, filsDroit);
		this.pere = new AtomicReference<>(pere);
		this.filsGauche = new AtomicReference<>(filsGauche);
		this.filsDroit = new AtomicReference<>(filsDroit);
	}

	public NoeudBinaireLockFree(int valeur, NoeudBinaire pere)
	{
		this(valeur, pere, null, null);
	}
	
	public NoeudBinaireLockFree(int valeur)
	{
		this(valeur, null, null, null);
	}
	
	private AtomicReference<NoeudBinaire> getAtomicFils(String fils)
	{
		Class<?> clazz = this.getClass();
		try 
		{
			Method method = clazz.getMethod("getAtomic"+fils, (Class<?>[]) null);
			return (AtomicReference<NoeudBinaire>)method.invoke(this, (Object[]) null);
		}
		//catch getMethod
		catch (NoSuchMethodException | SecurityException e) 
		{
			return null;
		}
		//catch invoke
		catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) 
		{
			return null;
		}
	}
	
	@Override
	protected boolean insert(int x, String fils)
	{
		AtomicReference<NoeudBinaire> noeudFils = this.getAtomicFils(fils);
		
		if(noeudFils.get() != null)
		{
			return noeudFils.get().insert(x);
		}
		else
		{
			NoeudBinaire noeudInsert = new NoeudBinaireLockFree(x, this);
			return noeudFils.compareAndSet(noeudFils.get(), noeudInsert);
		}
	}
	
	@Override
	protected boolean delete(int x, NoeudBinaire noeudASupprimer)
	{
		//On supprimer le noeud à supprimer depuis le noeud pere
		//Soit la valeur trouvée est dans la branche droite du noeud pere
		//Soit la valeur trouvée est dans la branche gauche du noeud pere
		if(noeudASupprimer.getPere().getFilsDroit() != null && 
		   noeudASupprimer.getPere().getFilsDroit().getValeur() == x)
		{
			NoeudBinaireLockFree pere = (NoeudBinaireLockFree)noeudASupprimer.getPere();
			AtomicReference<NoeudBinaire> fils = pere.getAtomicFilsDroit();
			return fils.compareAndSet(fils.get(), null);
		}
		else if(noeudASupprimer.getPere().getFilsGauche() != null && 
				noeudASupprimer.getPere().getFilsGauche().getValeur() == x)
		{
			NoeudBinaireLockFree pere = (NoeudBinaireLockFree)noeudASupprimer.getPere();
			AtomicReference<NoeudBinaire> fils = pere.getAtomicFilsGauche();
			return fils.compareAndSet(fils.get(), null);
		}
		else
		{
			return false;
		}
	}

	public AtomicReference<NoeudBinaire> getAtomicPere() {
		return this.pere;
	}
	
	public void setAtomicPere(AtomicReference<NoeudBinaire> pere) {
		this.pere = pere;
	}

	public AtomicReference<NoeudBinaire> getAtomicFilsGauche() {
		return this.filsGauche;
	}
	
	public void setAtomicFilsGauche(AtomicReference<NoeudBinaire> filsGauche) {
		this.filsGauche = filsGauche;
	}

	public AtomicReference<NoeudBinaire> getAtomicFilsDroit() {
		return this.filsDroit;
	}
	
	public void setAtomicFilsDroit(AtomicReference<NoeudBinaire> filsDroit) {
		this.filsDroit = filsDroit;
	}
	
	@Override
	public NoeudBinaire getPere() {
		return this.pere.get();
	}

	@Override
	public void setPere(NoeudBinaire pere) {
		this.pere.set(pere);
	}
	
	@Override
	public NoeudBinaire getFilsGauche() {
		return this.filsGauche.get();
	}

	@Override
	public void setFilsGauche(NoeudBinaire filsGauche) {
		this.filsGauche.set(filsGauche);
	}

	@Override
	public NoeudBinaire getFilsDroit() {
		return this.filsDroit.get();
	}

	@Override
	public void setFilsDroit(NoeudBinaire filsDroit) {
		this.filsDroit.set(filsDroit);
	}
	
	
}
