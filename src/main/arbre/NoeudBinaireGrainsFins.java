package arbre;

import java.util.TreeSet;

 /** 
 * Structure de données protéger par plusieurs verrous :
 * - Les fils droit et gauche
 * - Le père
 * 
 * @author Théo MAHAUDA, Mathieu PINEAU
 * @version 1.0
 */
public class NoeudBinaireGrainsFins extends NoeudBinaire
{
	public NoeudBinaireGrainsFins(int valeur)
	{
		super(valeur);
	}
	
	public NoeudBinaireGrainsFins(int valeur, NoeudBinaire pere)
	{
		super(valeur, pere);
	}
	
	public NoeudBinaireGrainsFins(int valeur, NoeudBinaire pere, NoeudBinaire filsGauche, NoeudBinaire filsDroit)
	{
		super(valeur, pere, filsGauche, filsDroit);
	}
	
	@Override
	public boolean delete(int x)
	{		
		TreeSet<Integer> list = new TreeSet<>();
		NoeudBinaire noeudASupprimer = this.search(x);
		boolean delete = false;
		
		//Si le noeud à supprimer n'existe pas et que ce n'est pas la racine
		if(noeudASupprimer != null && noeudASupprimer.getPere() != null)
		{	
			//Evite les interblocages -> voir théorème
			//On bloque le noeud à supprimer et son pere
			if(noeudASupprimer.getNumero() < noeudASupprimer.getPere().getNumero())
			{
				synchronized(noeudASupprimer)
				{
					synchronized(noeudASupprimer.getPere())
					{
						//On récupère toutes les valeurs des fils droit et gauche
						//du noeud à supprimer dans une liste ordonnée sans doublon
						list = noeudASupprimer.display();
						list.remove(noeudASupprimer.getValeur());
						delete = this.delete(x, noeudASupprimer);
					}
				}
			}
			else
			{
				synchronized(noeudASupprimer.getPere())
				{
					synchronized(noeudASupprimer)
					{
						//On récupère toutes les valeurs des fils droit et gauche
						//du noeud à supprimer dans une liste ordonnée sans doublon
						list = noeudASupprimer.display();
						list.remove(noeudASupprimer.getValeur());
						delete = this.delete(x, noeudASupprimer);
					}
				}
			}
			
			if(delete)
			{
				//On bloque l'arbre car il faut ajouter en priorité les valeurs supprimées
				synchronized(this)
				{
					//On ajoute les valeurs par ordre croissant
					for(int valeur : list)
					{
						this.insert(valeur);
					}
				}
				
				return true;
			}
			else
			{
				return false;
			}
		}
		else return false;
	}

	@Override
	public synchronized NoeudBinaire getPere() {
		return super.getPere();
	}
	
	@Override
	public synchronized void setPere(NoeudBinaire pere) {
		super.setPere(pere);
	}
	
	@Override
	public synchronized NoeudBinaire getFilsGauche() {
		return super.getFilsGauche();
	}
	
	@Override
	public synchronized void setFilsGauche(NoeudBinaire filsGauche) {
		super.setFilsGauche(filsGauche);
	}

	@Override
	public synchronized NoeudBinaire getFilsDroit() {
		return super.getFilsDroit();
	}

	@Override
	public synchronized void setFilsDroit(NoeudBinaire filsDroit) {
		super.setFilsDroit(filsDroit);
	}

}
