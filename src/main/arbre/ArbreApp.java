package arbre;

/**
* Application arbre binaire
* 
* @author Théo MAHAUDA, Mathieu PINEAU
*/
public class ArbreApp 
{	
	public static void main(String[] args) 
	{
		//Les ressources partagées entre les threads
		NoeudBinaire racineGrosGrains = new NoeudBinaireGrosGrains(10);
		NoeudBinaire racineGrainsFins = new NoeudBinaireGrainsFins(10);
		NoeudBinaire racineLockFree = new NoeudBinaireLockFree(10);
		
		demarrer("gros grains", racineGrosGrains, 5); //40ms
		demarrer("grains fins", racineGrainsFins, 5); //20ms
		demarrer("lock free", racineLockFree, 5); //20ms
	}
	
	private static void demarrer(String verrouillage, NoeudBinaire racine, int nombreThread)
	{
		long start;
		long end;
		Thread[] threads = new Thread[nombreThread];
		
		System.out.println("\nArbre " + verrouillage + "\n");
		
		start = System.currentTimeMillis();
		
		//On éxécute les threads
		for(int i=0; i<threads.length; i++)
		{
			threads[i] = new Thread(new ArbreBinaire(racine, "Arbre " + i));
			threads[i].start();
		}
		
		//On attend que les threads se terminent
		for(int i=0; i<threads.length; i++)
		{
			try 
			{
				threads[i].join();
			} 
			catch (InterruptedException e) {}
		}
		
		end = System.currentTimeMillis();
		
		System.out.println("\nTemps d'éxécution " + verrouillage + " : " + String.valueOf(end-start) + " ms\n");
	}
}
