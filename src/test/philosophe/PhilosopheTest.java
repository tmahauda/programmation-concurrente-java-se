package philosophe;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class PhilosopheTest {
	
	private Philosophe philosophe;
	
	@Before
	public void setUp() throws Exception 
	{
		this.philosophe = new Philosophe("test1");
		this.philosophe.setVoisinDroit(new Philosophe("test2"));
	}

	@Test
	public void testReflechir() 
	{
		this.philosophe.reflechir();
	}

	@Test
	public void testManger() 
	{
		this.philosophe.manger();
	}

}
