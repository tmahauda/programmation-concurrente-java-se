package arbre;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import java.util.TreeSet;

public class ArbreTest 
{
	private NoeudBinaire noeud;
	
	@Before
	public void setUp() throws Exception 
	{	
		//				
		//				 	  10
		//				/		   	 \
		//			  5			   	 15
		//			/	\		   /	\
		//		  4		 6	      14	16
		//		/		  \		  /		 \
		//	  2		  	  8		 12		 18
		//	/	\		/	\  /	\	/	\
		//	1	3		7	9 11	13 17	20
		
		this.noeud = new NoeudBinaire(10);
		
		//Partie gauche
		this.noeud.insert(5);
		this.noeud.insert(6);
		this.noeud.insert(8);
		this.noeud.insert(7);
		this.noeud.insert(9);
		this.noeud.insert(4);
		this.noeud.insert(2);
		this.noeud.insert(1);
		this.noeud.insert(3);
		
		//Partie droite
		this.noeud.insert(15);
		this.noeud.insert(16);
		this.noeud.insert(18);
		this.noeud.insert(17);
		this.noeud.insert(20);
		this.noeud.insert(14);
		this.noeud.insert(12);
		this.noeud.insert(11);
		this.noeud.insert(13);
	}

	@Test
	public void testDisplay() 
	{
		TreeSet<Integer> list = new TreeSet<>();
		
		for(int i=1; i<19; i++)
		{
			list.add(i);
		}
		list.add(20);
		assertTrue(this.noeud.display().containsAll(list));
	}
	
	@Test
	public void testContains1() 
	{
		assertTrue(this.noeud.contains(5));
	}
	
	@Test
	public void testContains2() 
	{
		assertFalse(this.noeud.contains(21));
	}
	
	@Test
	public void testSearch1()
	{
		assertEquals(20,this.noeud.search(20).getValeur());
	}
	
	@Test
	public void testSearch2()
	{
		assertNull(this.noeud.search(21));
	}

	//Insert d'une valeur égale à 20 = fils nul
	@Test
	public void testInsert1() 
	{
		assertFalse(this.noeud.insert(20));
	}
	
	//Insert d'une valeur plus grande à 20 = fils droit
	@Test
	public void testInsert2() 
	{
		assertTrue(this.noeud.insert(21));
		assertTrue(this.noeud.contains(21));
	}
	
	//Insert d'une valeur plus petit à 20 = fils gauche
	@Test
	public void testInsert3() 
	{
		assertTrue(this.noeud.insert(19));
		assertTrue(this.noeud.contains(19));
	}

	//Supprime une valeur à la racine = impossible
	@Test
	public void testDelete1() 
	{
		assertFalse(this.noeud.delete(10));
	}
	
	//Supprime une feuille d'un fils droit
	@Test
	public void testDelete2() 
	{
		//				
		//				 	  10
		//				/		   	 \
		//			  5			   	 15
		//			/	\		   /	\
		//		  4		 6	      14	16
		//		/		  \		  /		 \
		//	  2		  	  8		 12		 18
		//	/	\		/	\  /	\	/	
		//	1	3		7	9 11	13 17	
		
		assertTrue(this.noeud.delete(20));
		assertFalse(this.noeud.contains(20));
		
		TreeSet<Integer> list = new TreeSet<>();
		
		for(int i=1; i<19; i++)
		{
			list.add(i);
		}

		assertTrue(this.noeud.display().containsAll(list));
	}
	
	//Supprime une feuille d'un fils gauche
	@Test
	public void testDelete3() 
	{
		//				
		//				 	  10
		//				/		   	 \
		//			  5			   	 15
		//			/	\		   /	\
		//		  4		 6	      14	16
		//		/		  \		  /		 \
		//	  2		  	  8		 12		 18
		//		\		/	\  /	\	/	\
		//		3		7	9 11	13 17	20
		
		assertTrue(this.noeud.delete(1));
		assertFalse(this.noeud.contains(1));
		
		TreeSet<Integer> list = new TreeSet<>();
		
		for(int i=2; i<19; i++)
		{
			list.add(i);
		}
		list.add(20);

		assertTrue(this.noeud.display().containsAll(list));
	}
	
	
	//Supprime un noeud qui a fils droit et gauche
	@Test
	public void testDelete4() 
	{
		//				
		//				 	  10
		//				/		   	 \
		//			  5			   	 15
		//			/	\		   /   \
		//		  4		 6	      14	16
		//		/		  \		  /		  \
		//	  2		  	  8		 12		  17
		//	/	\		/	\  /	\		\
		//	1	3		7	9 11	13 		20
		
		assertTrue(this.noeud.delete(18));
		assertFalse(this.noeud.contains(18));
		
		NoeudBinaire noeud = this.noeud.search(16);
		assertNull(noeud.getFilsGauche());
		
		assertEquals(17,noeud.getFilsDroit().getValeur());
		assertNull(noeud.getFilsDroit().getFilsGauche());
		
		assertEquals(20,noeud.getFilsDroit().getFilsDroit().getValeur());
		assertNull(noeud.getFilsDroit().getFilsDroit().getFilsGauche());
	}

}
