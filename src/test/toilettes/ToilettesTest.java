package toilettes;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class ToilettesTest {

	private Toilettes toilette;
	
	@Before
	public void setUp() throws Exception 
	{
		this.toilette = new Toilettes(3);
	}

	@Test
	public void testEntrerHommesAttente() 
	{
		assertTrue(this.toilette.entrer(new Personne('M',"test1")));
		assertTrue(this.toilette.entrer(new Personne('M',"test2")));
		assertFalse(this.toilette.entrer(new Personne('F',"test3")));
		assertFalse(this.toilette.entrer(new Personne('M',"test4")));
		
		//Dans la file des toilettes
		assertEquals(2, this.toilette.getCptHommes());
		assertEquals(0, this.toilette.getCptFemmes());
		
		//Dans la file d'attente
		assertEquals(2, this.toilette.getPersonnesAttendre().size());
		
		//En tete de chaque file
		assertEquals("test1",this.toilette.getPersonnesEntrer().peek().getNom());
		assertEquals("test3",this.toilette.getPersonnesAttendre().peek().getNom());
	}
	
	@Test
	public void testEntrerHommesPlace() 
	{
		assertTrue(this.toilette.entrer(new Personne('M',"test1")));
		assertTrue(this.toilette.entrer(new Personne('M',"test2")));
		assertTrue(this.toilette.entrer(new Personne('M',"test3")));
		assertFalse(this.toilette.entrer(new Personne('M',"test4")));
		
		//Dans la file des toilettes
		assertEquals(3, this.toilette.getCptHommes());
		assertEquals(0, this.toilette.getCptFemmes());
		
		//Dans la file d'attente
		assertEquals(1, this.toilette.getPersonnesAttendre().size());
		
		//En tete de chaque file
		assertEquals("test1",this.toilette.getPersonnesEntrer().peek().getNom());
		assertEquals("test4",this.toilette.getPersonnesAttendre().peek().getNom());	
	}
	
	@Test
	public void testEntrerHommesUnisexe() 
	{
		assertTrue(this.toilette.entrer(new Personne('M',"test1")));
		assertTrue(this.toilette.entrer(new Personne('M',"test2")));
		assertFalse(this.toilette.entrer(new Personne('F',"test3")));
		
		//Dans la file des toilettes
		assertEquals(2, this.toilette.getCptHommes());
		assertEquals(0, this.toilette.getCptFemmes());
		
		//Dans la file d'attente
		assertEquals(1, this.toilette.getPersonnesAttendre().size());
		
		//En tete de chaque file
		assertEquals("test1",this.toilette.getPersonnesEntrer().peek().getNom());
		assertEquals("test3",this.toilette.getPersonnesAttendre().peek().getNom());
	}
	
	@Test
	public void testEntrerFemmesAttente() 
	{
		assertTrue(this.toilette.entrer(new Personne('F',"test1")));
		assertTrue(this.toilette.entrer(new Personne('F',"test2")));
		assertFalse(this.toilette.entrer(new Personne('M',"test3")));
		assertFalse(this.toilette.entrer(new Personne('F',"test4")));
		
		//Dans la file des toilettes
		assertEquals(0, this.toilette.getCptHommes());
		assertEquals(2, this.toilette.getCptFemmes());
		
		//Dans la file d'attente
		assertEquals(2, this.toilette.getPersonnesAttendre().size());
		
		//En tete de chaque file
		assertEquals("test1",this.toilette.getPersonnesEntrer().peek().getNom());
		assertEquals("test3",this.toilette.getPersonnesAttendre().peek().getNom());
	}
	
	@Test
	public void testEntrerFemmesPlace() 
	{
		assertTrue(this.toilette.entrer(new Personne('F',"test1")));
		assertTrue(this.toilette.entrer(new Personne('F',"test2")));
		assertTrue(this.toilette.entrer(new Personne('F',"test3")));
		assertFalse(this.toilette.entrer(new Personne('F',"test4")));
		
		//Dans la file des toilettes
		assertEquals(0, this.toilette.getCptHommes());
		assertEquals(3, this.toilette.getCptFemmes());
		
		//Dans la file d'attente
		assertEquals(1, this.toilette.getPersonnesAttendre().size());
		
		//En tete de chaque file
		assertEquals("test1",this.toilette.getPersonnesEntrer().peek().getNom());
		assertEquals("test4",this.toilette.getPersonnesAttendre().peek().getNom());	
	}
	
	@Test
	public void testEntrerFemmesUnisexe() 
	{
		assertTrue(this.toilette.entrer(new Personne('F',"test1")));
		assertTrue(this.toilette.entrer(new Personne('F',"test2")));
		assertFalse(this.toilette.entrer(new Personne('M',"test3")));
		
		//Dans la file des toilettes
		assertEquals(0, this.toilette.getCptHommes());
		assertEquals(2, this.toilette.getCptFemmes());
		
		//Dans la file d'attente
		assertEquals(1, this.toilette.getPersonnesAttendre().size());
		
		//En tete de chaque file
		assertEquals("test1",this.toilette.getPersonnesEntrer().peek().getNom());
		assertEquals("test3",this.toilette.getPersonnesAttendre().peek().getNom());
	}

	//Un homme qui n'est pas encore dans les toilettes
	@Test
	public void testSortirHommes1() 
	{
		Personne test1 = new Personne('M',"test1");
		Personne test2 = new Personne('M',"test2");
		Personne test3 = new Personne('M',"test3");
		
		assertTrue(this.toilette.entrer(test1));
		assertTrue(this.toilette.entrer(test2));
		
		//test3 ne sort pas car n'existe pas
		//assertFalse(this.toilette.sortir(test3));-->boucle infini car implémentation d'un wait()
		
		assertEquals(2, this.toilette.getCptHommes());
		assertEquals(0, this.toilette.getCptFemmes());
		
		//FILE TOILETTE
		
		//test3 n'est plus présent dans la file des toilettes
		assertFalse(this.toilette.getPersonnesEntrer().contains(test3));
		
		//test1 est en tete de file de toilettes
		assertEquals(test1,this.toilette.getPersonnesEntrer().getFirst());
		
		//test2 est en queue de file de toilettes
		assertEquals(test2,this.toilette.getPersonnesEntrer().getLast());
		
		//2 personnes dans les toilettes
		assertEquals(2,this.toilette.getPersonnesEntrer().size());
		
		//FILE ATTENTE
		
		//0 personne dans la file d'attente
		assertEquals(0,this.toilette.getPersonnesAttendre().size());
	}
	
	//Un homme sort et personne dans file d'attente
	@Test
	public void testSortirHommes2() 
	{
		Personne test1 = new Personne('M',"test1");
		Personne test2 = new Personne('M',"test2");
		Personne test3 = new Personne('M',"test3");
		
		assertTrue(this.toilette.entrer(test1));
		assertTrue(this.toilette.entrer(test2));
		assertTrue(this.toilette.entrer(test3));
		
		//test1 sort
		assertTrue(this.toilette.sortir(test1));
		
		assertEquals(2, this.toilette.getCptHommes());
		assertEquals(0, this.toilette.getCptFemmes());
		
		//FILE TOILETTE
		
		//test1 n'est plus présent dans la file des toilettes
		assertFalse(this.toilette.getPersonnesEntrer().contains(test1));
		
		//test2 est en tete de file de toilettes
		assertEquals(test2,this.toilette.getPersonnesEntrer().getFirst());
		
		//test3 est en queue de file de toilettes
		assertEquals(test3,this.toilette.getPersonnesEntrer().getLast());
		
		//2 personnes dans les toilettes
		assertEquals(2,this.toilette.getPersonnesEntrer().size());
		
		//FILE ATTENTE
		
		//0 personne dans la file d'attente
		assertEquals(0,this.toilette.getPersonnesAttendre().size());
	}
	
	//Un homme (test4) tente de rentrer après avoir attendu dans la file d'attente
	//--> test4 peut rentrer car unisexe
	//--> test5 et 6 ne peuvent pas rentrer car pas de place
	@Test
	public void testSortirHommes3() 
	{
		Personne test1 = new Personne('M',"test1");
		Personne test2 = new Personne('M',"test2");
		Personne test3 = new Personne('M',"test3");
		Personne test4 = new Personne('M',"test4");
		Personne test5 = new Personne('M',"test5");
		Personne test6 = new Personne('M',"test6");
		
		assertTrue(this.toilette.entrer(test1));
		assertTrue(this.toilette.entrer(test2));
		assertTrue(this.toilette.entrer(test3));
		assertFalse(this.toilette.entrer(test4));
		assertFalse(this.toilette.entrer(test5));
		assertFalse(this.toilette.entrer(test6));
		
		//test1 sort et test4 entre
		assertTrue(this.toilette.sortir(test1));
		
		assertEquals(3, this.toilette.getCptHommes());
		assertEquals(0, this.toilette.getCptFemmes());
		
		//FILE TOILETTE
		
		//test1 n'est plus présent dans la file des toilettes
		assertFalse(this.toilette.getPersonnesEntrer().contains(test1));
		
		//test2 est en tete de file de toilettes
		assertEquals(test2,this.toilette.getPersonnesEntrer().getFirst());
		
		//test4 est en queue de file de toilettes
		assertEquals(test4,this.toilette.getPersonnesEntrer().getLast());
		
		//3 personnes dans les toilettes
		assertEquals(3,this.toilette.getPersonnesEntrer().size());
		
		//FILE ATTENTE
		
		//test4 n'est plus présent dans la file d'attente
		assertFalse(this.toilette.getPersonnesAttendre().contains(test4));
		
		//test5 est en tete de file d'attente
		assertEquals(test5,this.toilette.getPersonnesAttendre().getFirst());
				
		//test6 est en queue de file d'attente
		assertEquals(test6,this.toilette.getPersonnesAttendre().getLast());
		
		//2 personne dans la file d'attente
		assertEquals(2,this.toilette.getPersonnesAttendre().size());
	}
	
	//Une femme (test4) tente de rentrer après avoir attendu dans la file d'attente
	//--> test4 ne peut pas rentrer car non unisexe
	//--> test5 et 6 ne peuvent pas rentrer car pas de place
	@Test
	public void testSortirHommes4() 
	{
		Personne test1 = new Personne('M',"test1");
		Personne test2 = new Personne('M',"test2");
		Personne test3 = new Personne('M',"test3");
		Personne test4 = new Personne('F',"test4");
		Personne test5 = new Personne('F',"test5");
		Personne test6 = new Personne('F',"test6");
		
		assertTrue(this.toilette.entrer(test1));
		assertTrue(this.toilette.entrer(test2));
		assertTrue(this.toilette.entrer(test3));
		assertFalse(this.toilette.entrer(test4));
		assertFalse(this.toilette.entrer(test5));
		assertFalse(this.toilette.entrer(test6));
		
		//test1 sort et test4 n'entre pas
		assertTrue(this.toilette.sortir(test1));
		
		assertEquals(2, this.toilette.getCptHommes());
		assertEquals(0, this.toilette.getCptFemmes());
		
		//FILE TOILETTE
		
		//test1 n'est plus présent dans la file des toilettes
		assertFalse(this.toilette.getPersonnesEntrer().contains(test1));
		
		//test2 est en tete de file de toilettes
		assertEquals(test2,this.toilette.getPersonnesEntrer().getFirst());
		
		//test3 est en queue de file de toilettes
		assertEquals(test3,this.toilette.getPersonnesEntrer().getLast());
		
		//2 personnes dans les toilettes
		assertEquals(2,this.toilette.getPersonnesEntrer().size());
		
		//FILE ATTENTE
		
		//test4 est présent dans la file d'attente
		assertTrue(this.toilette.getPersonnesAttendre().contains(test4));
		
		//test4 est en tete de file d'attente
		assertEquals(test4,this.toilette.getPersonnesAttendre().getFirst());
				
		//test6 est en queue de file d'attente
		assertEquals(test6,this.toilette.getPersonnesAttendre().getLast());
		
		//3 personne dans la file d'attente
		assertEquals(3,this.toilette.getPersonnesAttendre().size());
	}
	
	//Toutes les femmes rentrent de la file d'attente lorsque 
	//tous les hommes sont sorties des toilettes
	@Test
	public void testSortirHommes5() 
	{
		Personne test1 = new Personne('M',"test1");
		Personne test2 = new Personne('M',"test2");
		Personne test3 = new Personne('M',"test3");
		Personne test4 = new Personne('F',"test4");
		Personne test5 = new Personne('F',"test5");
		Personne test6 = new Personne('F',"test6");
		Personne test7 = new Personne('F',"test7");
		
		assertTrue(this.toilette.entrer(test1));
		assertTrue(this.toilette.entrer(test2));
		assertTrue(this.toilette.entrer(test3));
		assertFalse(this.toilette.entrer(test4));
		assertFalse(this.toilette.entrer(test5));
		assertFalse(this.toilette.entrer(test6));
		assertFalse(this.toilette.entrer(test7));
		
		//test1 sort et les femmes ne peuvent pas rentrer
		assertTrue(this.toilette.sortir(test1));
		//test2 sort et les femmes ne peuvent pas rentrer
		assertTrue(this.toilette.sortir(test2));
		//test3 sort et toutes les femmes rentrent (sauf test7)
		assertTrue(this.toilette.sortir(test3));
		
		assertEquals(0, this.toilette.getCptHommes());
		assertEquals(3, this.toilette.getCptFemmes());
		
		//FILE TOILETTE
		
		//test1, 2 et 3 ne sont plus présent dans la file des toilettes
		assertFalse(this.toilette.getPersonnesEntrer().contains(test1));
		assertFalse(this.toilette.getPersonnesEntrer().contains(test2));
		assertFalse(this.toilette.getPersonnesEntrer().contains(test3));
		
		//test4 est en tete de file de toilettes
		assertEquals(test4,this.toilette.getPersonnesEntrer().getFirst());
		
		//test6 est en queue de file de toilettes
		assertEquals(test6,this.toilette.getPersonnesEntrer().getLast());
		
		//3 personnes dans les toilettes
		assertEquals(3,this.toilette.getPersonnesEntrer().size());
		
		//FILE ATTENTE
		
		//test4, 5 et 6 ne sont pas présent dans la file d'attente
		assertFalse(this.toilette.getPersonnesAttendre().contains(test4));
		assertFalse(this.toilette.getPersonnesAttendre().contains(test5));
		assertFalse(this.toilette.getPersonnesAttendre().contains(test6));
			
		//test7 est en tete de file d'attente
		assertEquals(test7,this.toilette.getPersonnesAttendre().getFirst());		
			
		//test7 est en queue de file d'attente
		assertEquals(test7,this.toilette.getPersonnesAttendre().getLast());
				
		//1 personne dans la file d'attente
		assertEquals(1,this.toilette.getPersonnesAttendre().size());
	}

	//Une femme qui n'est pas encore dans les toilettes
	@Test
	public void testSortirFemmes1() 
	{
		Personne test1 = new Personne('F',"test1");
		Personne test2 = new Personne('F',"test2");
		Personne test3 = new Personne('F',"test3");
		
		assertTrue(this.toilette.entrer(test1));
		assertTrue(this.toilette.entrer(test2));
		
		//test3 ne sort pas car n'existe pas
		//assertFalse(this.toilette.sortir(test3));-->boucle infini car implémentation d'un wait()
		
		assertEquals(0, this.toilette.getCptHommes());
		assertEquals(2, this.toilette.getCptFemmes());
		
		//FILE TOILETTE
		
		//test3 n'est plus présent dans la file des toilettes
		assertFalse(this.toilette.getPersonnesEntrer().contains(test3));
		
		//test1 est en tete de file de toilettes
		assertEquals(test1,this.toilette.getPersonnesEntrer().getFirst());
		
		//test2 est en queue de file de toilettes
		assertEquals(test2,this.toilette.getPersonnesEntrer().getLast());
		
		//2 personnes dans les toilettes
		assertEquals(2,this.toilette.getPersonnesEntrer().size());
		
		//FILE ATTENTE
		
		//0 personne dans la file d'attente
		assertEquals(0,this.toilette.getPersonnesAttendre().size());
	}
	
	//Une femme sort et personne dans file d'attente
	@Test
	public void testSortirFemmes2() 
	{
		Personne test1 = new Personne('F',"test1");
		Personne test2 = new Personne('F',"test2");
		Personne test3 = new Personne('F',"test3");
		
		assertTrue(this.toilette.entrer(test1));
		assertTrue(this.toilette.entrer(test2));
		assertTrue(this.toilette.entrer(test3));
		
		//test1 sort
		assertTrue(this.toilette.sortir(test1));
		
		assertEquals(0, this.toilette.getCptHommes());
		assertEquals(2, this.toilette.getCptFemmes());
		
		//FILE TOILETTE
		
		//test1 n'est plus présent dans la file des toilettes
		assertFalse(this.toilette.getPersonnesEntrer().contains(test1));
		
		//test2 est en tete de file de toilettes
		assertEquals(test2,this.toilette.getPersonnesEntrer().getFirst());
		
		//test3 est en queue de file de toilettes
		assertEquals(test3,this.toilette.getPersonnesEntrer().getLast());
		
		//2 personnes dans les toilettes
		assertEquals(2,this.toilette.getPersonnesEntrer().size());
		
		//FILE ATTENTE
		
		//0 personne dans la file d'attente
		assertEquals(0,this.toilette.getPersonnesAttendre().size());
	}
	
	//Une femme (test4) tente de rentrer après avoir attendu dans la file d'attente
	//--> test4 peut rentrer car unisexe
	//--> test5 et 6 ne peuvent pas rentrer car pas de place
	@Test
	public void testSortirFemmes3() 
	{
		Personne test1 = new Personne('F',"test1");
		Personne test2 = new Personne('F',"test2");
		Personne test3 = new Personne('F',"test3");
		Personne test4 = new Personne('F',"test4");
		Personne test5 = new Personne('F',"test5");
		Personne test6 = new Personne('F',"test6");
		
		assertTrue(this.toilette.entrer(test1));
		assertTrue(this.toilette.entrer(test2));
		assertTrue(this.toilette.entrer(test3));
		assertFalse(this.toilette.entrer(test4));
		assertFalse(this.toilette.entrer(test5));
		assertFalse(this.toilette.entrer(test6));
		
		//test1 sort et test4 entre
		assertTrue(this.toilette.sortir(test1));
		
		assertEquals(0, this.toilette.getCptHommes());
		assertEquals(3, this.toilette.getCptFemmes());
		
		//FILE TOILETTE
		
		//test1 n'est plus présent dans la file des toilettes
		assertFalse(this.toilette.getPersonnesEntrer().contains(test1));
		
		//test2 est en tete de file de toilettes
		assertEquals(test2,this.toilette.getPersonnesEntrer().getFirst());
		
		//test4 est en queue de file de toilettes
		assertEquals(test4,this.toilette.getPersonnesEntrer().getLast());
		
		//3 personnes dans les toilettes
		assertEquals(3,this.toilette.getPersonnesEntrer().size());
		
		//FILE ATTENTE
		
		//test4 n'est plus présent dans la file d'attente
		assertFalse(this.toilette.getPersonnesAttendre().contains(test4));
		
		//test5 est en tete de file d'attente
		assertEquals(test5,this.toilette.getPersonnesAttendre().getFirst());
				
		//test6 est en queue de file d'attente
		assertEquals(test6,this.toilette.getPersonnesAttendre().getLast());
		
		//2 personne dans la file d'attente
		assertEquals(2,this.toilette.getPersonnesAttendre().size());
	}
	
	//Un homme (test4) tente de rentrer après avoir attendu dans la file d'attente
	//--> test4 ne peut pas rentrer car non unisexe
	//--> test5 et 6 ne peuvent pas rentrer car pas de place
	@Test
	public void testSortirFemmes4() 
	{
		Personne test1 = new Personne('F',"test1");
		Personne test2 = new Personne('F',"test2");
		Personne test3 = new Personne('F',"test3");
		Personne test4 = new Personne('M',"test4");
		Personne test5 = new Personne('M',"test5");
		Personne test6 = new Personne('M',"test6");
		
		assertTrue(this.toilette.entrer(test1));
		assertTrue(this.toilette.entrer(test2));
		assertTrue(this.toilette.entrer(test3));
		assertFalse(this.toilette.entrer(test4));
		assertFalse(this.toilette.entrer(test5));
		assertFalse(this.toilette.entrer(test6));
		
		//test1 sort et test4 n'entre pas
		assertTrue(this.toilette.sortir(test1));
		
		assertEquals(0, this.toilette.getCptHommes());
		assertEquals(2, this.toilette.getCptFemmes());
		
		//FILE TOILETTE
		
		//test1 n'est plus présent dans la file des toilettes
		assertFalse(this.toilette.getPersonnesEntrer().contains(test1));
		
		//test2 est en tete de file de toilettes
		assertEquals(test2,this.toilette.getPersonnesEntrer().getFirst());
		
		//test3 est en queue de file de toilettes
		assertEquals(test3,this.toilette.getPersonnesEntrer().getLast());
		
		//2 personnes dans les toilettes
		assertEquals(2,this.toilette.getPersonnesEntrer().size());
		
		//FILE ATTENTE
		
		//test4 est présent dans la file d'attente
		assertTrue(this.toilette.getPersonnesAttendre().contains(test4));
		
		//test4 est en tete de file d'attente
		assertEquals(test4,this.toilette.getPersonnesAttendre().getFirst());
				
		//test6 est en queue de file d'attente
		assertEquals(test6,this.toilette.getPersonnesAttendre().getLast());
		
		//3 personne dans la file d'attente
		assertEquals(3,this.toilette.getPersonnesAttendre().size());
	}
	
	//Tous les hommes rentrent de la file d'attente lorsque 
	//toutes les femmes sont sorties des toilettes
	@Test
	public void testSortirFemmes5() 
	{
		Personne test1 = new Personne('F',"test1");
		Personne test2 = new Personne('F',"test2");
		Personne test3 = new Personne('F',"test3");
		Personne test4 = new Personne('M',"test4");
		Personne test5 = new Personne('M',"test5");
		Personne test6 = new Personne('M',"test6");
		Personne test7 = new Personne('M',"test7");
		
		assertTrue(this.toilette.entrer(test1));
		assertTrue(this.toilette.entrer(test2));
		assertTrue(this.toilette.entrer(test3));
		assertFalse(this.toilette.entrer(test4));
		assertFalse(this.toilette.entrer(test5));
		assertFalse(this.toilette.entrer(test6));
		assertFalse(this.toilette.entrer(test7));
		
		//test1 sort et les femmes ne peuvent pas rentrer
		assertTrue(this.toilette.sortir(test1));
		//test2 sort et les femmes ne peuvent pas rentrer
		assertTrue(this.toilette.sortir(test2));
		//test3 sort et toutes les femmes rentrent (sauf test7)
		assertTrue(this.toilette.sortir(test3));
		
		assertEquals(3, this.toilette.getCptHommes());
		assertEquals(0, this.toilette.getCptFemmes());
		
		//FILE TOILETTE
		
		//test1, 2 et 3 ne sont plus présent dans la file des toilettes
		assertFalse(this.toilette.getPersonnesEntrer().contains(test1));
		assertFalse(this.toilette.getPersonnesEntrer().contains(test2));
		assertFalse(this.toilette.getPersonnesEntrer().contains(test3));
		
		//test4 est en tete de file de toilettes
		assertEquals(test4,this.toilette.getPersonnesEntrer().getFirst());
		
		//test6 est en queue de file de toilettes
		assertEquals(test6,this.toilette.getPersonnesEntrer().getLast());
		
		//3 personnes dans les toilettes
		assertEquals(3,this.toilette.getPersonnesEntrer().size());
		
		//FILE ATTENTE
		
		//test4, 5 et 6 ne sont pas présent dans la file d'attente
		assertFalse(this.toilette.getPersonnesAttendre().contains(test4));
		assertFalse(this.toilette.getPersonnesAttendre().contains(test5));
		assertFalse(this.toilette.getPersonnesAttendre().contains(test6));
			
		//test7 est en tete de file d'attente
		assertEquals(test7,this.toilette.getPersonnesAttendre().getFirst());		
			
		//test7 est en queue de file d'attente
		assertEquals(test7,this.toilette.getPersonnesAttendre().getLast());
				
		//1 personne dans la file d'attente
		assertEquals(1,this.toilette.getPersonnesAttendre().size());
	}
	
}
